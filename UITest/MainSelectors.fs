﻿namespace UITest

module MainSelectors =
    let loginUserInput = "input[data-ng-model='user.userid']"
    let loginPasswordInput = "input[data-ng-model='user.pw']"
    let loginSubmitBtn = "button[ng-click='login(user); clickBtn()']"
    let loginErrorMsgDiv = "div[class='alert alert-danger']"
    let loginErrorMsgCloseBtn = "button[class='close']"
    let loginDiv = ".login-on"


