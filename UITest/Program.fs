﻿open canopy
open runner
open System
open UITest.MainSelectors
open UITest.PlatformSelectors

chromeDir <- "/usr/local/bin" // In case /usr/bin is not chrome's default path
autoPinBrowserRightOnLaunch <- false
//pin FullScreen 
elementTimeout <- 10.0
compareTimeout <- 10.0
pageTimeout <- 10.0

let mainUrl = "https://test.gatecoin.com"
let platformUrl = "https://test.gatecoin.com/platform"
let username = ""
let password = ""

start chrome

context "Main Page Tests"
"Incorrect User Login" &&& (fun _ ->

    let invalidUser = "invalid@user.com"
    let invalidPassword = "invalid@password.com"

    url mainUrl
    click loginDiv
    disabled loginSubmitBtn
    loginUserInput << invalidUser
    loginPasswordInput << invalidPassword
    enabled loginSubmitBtn
    click loginSubmitBtn

    displayed loginErrorMsgDiv
    click loginErrorMsgCloseBtn
    notDisplayed loginErrorMsgDiv

    clear loginUserInput
    clear loginPasswordInput
    disabled loginSubmitBtn

    ()
)
"Successful User Login" &&& (fun _ ->

    let redirectUrl = "https://test.gatecoin.com/platform"

    url mainUrl
    click loginDiv
    loginUserInput << username
    loginPasswordInput << password
    click loginSubmitBtn
    on redirectUrl  

    ()
)

context "Platform Tests"
"Insufficient funds trade" &&& (fun _ ->

    let tradeAmount = "999999999999"

    url platformUrl
    click tradeBtn

    tradePriceInput << tradeAmount
    tradeQuantityInput << tradeAmount
    click tradeSubmitOrderBtn

    displayed tradeErrorMsgDiv

    ()
)
"Insufficient funds Instant Buy/Sell" &&& (fun _ ->

    let tradeAmount = "999999999999"
    let givenCurrency = "BTC"
    let receivedCurrency = "USD"

    url platformUrl
    click ibsBtn

    ibsPayInput << tradeAmount
    ibsPayCurrency << givenCurrency
    ibsReceiveCurrency << receivedCurrency
    notDisplayed ibsSummaryDiv
    click ibsGetPriceBtn
    displayed ibsSummaryDiv
    click ibsSubmitOrderBtn

    displayed ibsErrorMsgDiv

    ()
)


run()
printfn "press [enter] to exit"
System.Console.ReadLine() |> ignore
quit()
