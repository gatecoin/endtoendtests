﻿namespace UITest

module PlatformSelectors = 
    // Trade Tab
    let tradeBtn = "Trade";
    let tradePriceInput = "input[ng-model='order.price']"
    let tradeQuantityInput = "input[ng-model='order.quantity']"
    let tradeTotalInput = "input[ng-model='order.total']"
    let tradeSubmitOrderBtn = "button[submit-spinner='addOrder(order);']"
    let tradeErrorMsgDiv = ".alert.alert-danger"

    // Instant Buy/Sell Tab
    let ibsBtn = "Instant Buy/Sell"
    let ibsPayInput = "input[ng-model='quote.input']"
    let ibsPayCurrency = "select[ng-model='quote.sellCurrency']"
    let ibsReceiveInput = "input[ng-model='quote.output']"
    let ibsReceiveCurrency = "select[ng-model='quote.selectedCurrency']"
    let ibsGetPriceBtn = "button[translate='getPrice']"
    let ibsSubmitOrderBtn = "button[submit-spinner='postPurchaseOrder();']"
    let ibsSummaryDiv = "div[ng-show='quickChangeDisplayTimer']"
    let ibsErrorMsgDiv = ".alert.alert-danger"
